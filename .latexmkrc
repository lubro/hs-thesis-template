@default_files = ("thesis.tex");
$xelatex = "xelatex -shell-escape";

$pdf_mode = 5;
$dvi_mode = 0;

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
      system "makeglossaries -q '$_[0]'";
        }
          else {
              system "makeglossaries '$_[0]'";
                };
                }
