% vim: set spl=de: Setup
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hs-aalen}
\DeclareOption{twoside}{
    \PassOptionsToClass{twoside}{scrreprt}
    \PassOptionsToPackage{twoside}{geometry}
}
\DeclareOption{german}{
    \def \german {1}
}
\ProcessOptions\relax

\LoadClass[12pt, a4paper]{scrreprt}

\RequirePackage{todonotes}
\RequirePackage{lipsum}

\RequirePackage[top=3cm, bottom=4cm,inner=2cm,outer=3cm]{geometry}
\RequirePackage[hidelinks]{hyperref}
\ifx\german\undefined
    \RequirePackage{babel}
\else
    \RequirePackage[ngerman]{babel}
\fi

\RequirePackage{colortbl}
% Font
\RequirePackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}
\setmainfont{Arial}
\setkomafont{disposition}{\normalfont\bfseries}
\setkomafont{caption}{\normalfont}

% Paragraph
\setlength{\parskip}{6pt}
\setlength{\parindent}{0pt}


% Images
\RequirePackage{graphicx}
\RequirePackage[export]{adjustbox}

% TabularX
\RequirePackage{tabularx}
\RequirePackage{multirow}
\RequirePackage{rotating}

% Beautiful colors
\RequirePackage{xcolor}
\definecolor{mainBlue}{RGB}{0,104,180}
\definecolor{darkBlue}{RGB}{0,58,108}
\definecolor{mainPurble}{RGB}{68, 85, 170}
\definecolor{mainGreen}{RGB}{0, 181, 165}
\definecolor{realGray}{RGB}{200, 200, 200}

% Headlines Koma Script
\setkomafont{chapter}{\normalfont\color{mainBlue}\bfseries\Huge}
\RedeclareSectionCommand[
  %runin=false,
  afterindent=false,
  beforeskip=1\baselineskip,
  afterskip=2\baselineskip]{chapter}

\setkomafont{section}{\normalfont\color{mainBlue}\bfseries\LARGE}
\setkomafont{subsection}{\normalfont\color{darkBlue}\bfseries\Large}
\setkomafont{subsubsection}{\normalfont\color{darkBlue}\bfseries\large}

% Code Highlighting
\RequirePackage{minted}
%\definecolor{mitedBG}{RGB}{230, 220, 210}
\definecolor{mitedBG}{RGB}{210, 220, 230}
\usemintedstyle{perldoc}
\setminted{
    xleftmargin=2em,
    framesep=2mm,
    bgcolor=mitedBG,
    baselinestretch=1.2,
    fontsize=\normalsize,
    linenos
}

% Multicols
\RequirePackage{multicol}

% Header and footer
\setlength{\headheight}{28pt}
\RequirePackage[automark]{scrlayer-scrpage}
\clearpairofpagestyles
\automark[chapter]{chapter}
\ohead*{\headmark}
\ihead*{\includegraphics[height=0.7cm]{img/htw-aalen.pdf}}
\ifoot[]{}
\ofoot[]{}
\cfoot[\pagemark]{\pagemark}

%Abstract
\renewenvironment{abstract}{
    \vspace*{3cm}
    \centerline{\textbf{\textcolor{mainBlue}{\Huge{\abstractname}}}}
    \vspace*{1cm}
}{}
\RequirePackage{afterpage}

\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \newpage}
% Glossary

\RequirePackage[acronym]{glossaries}
\renewcommand*{\acronymname}{Glossary of acronyms}
\makeglossaries
%\makenoidxglossaries
\newacronym{bsp}{Bsp.}{Beispiel}

\RequirePackage{csquotes}
% Bibliography
\RequirePackage[backend=biber]{biblatex}
\setcounter{biburlnumpenalty}{9000}
\setcounter{biburllcpenalty}{9000}
\setcounter{biburlucpenalty}{9000}

\addbibresource{resources/resources.bib}

% QR-Codes
\RequirePackage{qrcode}

% Code highlights:
\newcommand{\cs}[1]{\normalsize{\textbf{\texttt{#1}}}}

% ###############################################
% ############## Title Page #####################
% ###############################################
\newcommand{\printTitlePage}{
\newgeometry{margin=1in}
\thispagestyle{empty}
\begin{center}
    \vspace*{3cm}
    \textbf{\Huge{\textcolor{mainBlue}{\title}}}\\
    \vspace{0.5cm}
    \textbf{\LARGE{\textcolor{darkBlue}{\subtitle}}}\\
    \vspace{7cm}
    \includegraphics[width=0.7\textwidth]{img/htw-aalen.pdf}\\
    \vspace{0.5cm}
    \large{\authorname, \authorinfo}\\
    \vspace{0.5cm}
    \large{\subject}\\
    \vspace{5cm}
    \begin{tabularx}{\textwidth}{l X r}
        \ifx\german\undefined \large{Supervised by:} \else \large{Betreut durch:}\fi & ~ & \ifx\german\undefined \large{Second Reviewer:}\else\large{Zweitkorrektur:} \fi\\
        \large{\supervisor} & ~ & \large{\secondreviewer}\\
    \end{tabularx}
\end{center}
\restoregeometry
\newpage
%\blankpage
\cleardoublepage
\setcounter{page}{1}
}
